package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutTest {

    @Test
    public void testdesc() throws Exception {

        String msg= new About().desc();
        assertEquals("About Test", "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", msg);
    }
}
