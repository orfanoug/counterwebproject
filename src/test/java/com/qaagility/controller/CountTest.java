package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CountTest {

    @Test
    public void methodD() throws Exception {

        int result1= new Count().methodD(2,1);
        assertEquals("Count Test 1", 2, result1);

	int result2 = new Count().methodD(2,0);
	assertEquals("Count Test 2", Integer.MAX_VALUE,result2);
    }
}

